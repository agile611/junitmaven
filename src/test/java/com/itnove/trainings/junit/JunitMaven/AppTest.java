package com.itnove.trainings.junit.JunitMaven;

import static org.junit.Assert.assertTrue;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        assertTrue(App.getWish().equals("Hello"));
    }
}

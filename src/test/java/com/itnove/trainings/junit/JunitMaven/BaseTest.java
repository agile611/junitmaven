package com.itnove.trainings.junit.JunitMaven;

import org.junit.After;
import org.junit.Before;

import java.io.IOException;


/**
 * Created by guillem on 29/02/16.
 */
public class BaseTest {

    @Before
    public void setUp() throws IOException {
        System.out.println("Right inside the Before");
    }

    @After
    public void tearDown() {
        System.out.println("Right inside the After");
    }

}
